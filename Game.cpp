#include "Game.h"

Game::Game(string levelFileName) {

}

Game::~Game() {

}

void Game::menu() {
	char choice;
	cout << "  MAIN MENU  \n";
	cout << "=============\n";
	cout << "P: PLAY\n";
	cout << "Q: QUIT TO TERMINAL\n";
	cout << "> ";
	cin >> choice;

	switch(choice) {
		case 'P':
		case 'p':
			play();
			break;
		case 'Q':
		case 'q':
			break;
		default:
			system("clear");
			menu();
			break;
	}
}

//play() sets up the game
void Game::play() {
	system("clear");
	_player.init("Jacob", 10, 10, 10, 0); //initalize the player as Jacob, 10HP, 10AP, 10Dp, and 0gp.
	_level.load("levelone.txt", _player); //load the level, and pass the player as a reference.
	_level.print(); //print the level loaded by _level.load();
}
