//PLAYER CLASS
#ifndef PLAYER_H
#define PLAYER_H
#include <iostream>
#include <string>
using namespace std;

class Player {
	public:
		Player();
		~Player();
		void init(string name, int health, int attack, int defense, int gold);

		//setters
		void setPosition(int x, int y); //set the coordinates of the player

		//getters
		void getPosition(int &x, int &y); //get the coordinates of the player using reference variables

	private:
	//properties
		string _name;
		int _health;
		int _attack;
		int _defense;
		int _gold;
	//position
		int _x;
		int _y;
};

#endif // PLAYER_H
