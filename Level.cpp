#include "Level.h"

Level::Level() {

	}

Level::~Level() {

	}

void Level::load(string fileName, Player &player) {
//loads the level
	ifstream file;
	file.open(fileName);
	if(file.fail()){
		perror(fileName.c_str());
		exit(-1);
	}

string line; //line of the file
while(getline(file, line)) { //while there's still lines in the file...
	_levelData.push_back(line); //add them to the vector of level data...
	}
	file.close(); //and then close the file.

	//process the level
	char tile; //a monster, a '.', a '#', or the '@'

	for(int i = 0; i < _levelData.size(); i++) { //loop through every line
		for(int j = 0; j < _levelData[i].size(); j++) { //loop through every character
			tile = _levelData[i][j]; //the tile equals a position inside the level data, at coords x,y (i, j)

			switch(tile) { //decide what to do with the tile of a specific type.
				case '@':
				player.setPosition(j, i); //sets the player's position to the current position on the screen. (x, y)
				//Allow the player to move to an empty space
					break;
				case '$':

					break;
			}
		}
	}

}

void Level::print() {
	for(int i = 0; i < _levelData.size(); i++){
		printf("%s\n", _levelData[i].c_str());
	}
	printf("\n");
}
