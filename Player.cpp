#include "Player.h"

Player::Player() {
	_x = 0;
	_y = 0;
}

Player::~Player() {

}

void Player::init(std::string name, int health, int attack, int defense, int gold) {
	_name = name;
	_health = health;
	_attack = attack;
	_defense = defense;
	_gold = gold;
}

void Player::setPosition(int x, int y) {
	_x = x;
	_y = y;
}

void Player::getPosition(int& x, int& y) {
	x = _x;
	y = _y;
}
