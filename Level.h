#ifndef LEVEL_H
#define LEVEL_H
#include <string>
#include <ncurses.h>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <fstream>
#include "Player.h"
using namespace std;

class Level {
	public:
		Level();
		~Level();
		void load(string fileName, Player &player);
		void print();

	private:
		vector<string> _levelData; //contains the level data (i.e lines in the level file)
		Player _player;
	};

#endif
