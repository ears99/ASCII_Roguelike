#ifndef GAME_H
#define GAME_H
#include <iostream>
#include <string>
#include <ncurses.h>

#include "Level.h"
#include "Player.h"
using namespace std;

enum class GameState{PLAY, QUIT, MENU};

class Game {
	public:
		Game(string levelFileName);
		~Game();

		void play();
		void menu();

	private:
		Player _player;
		Level _level;
};

#endif
